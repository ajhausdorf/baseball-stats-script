import re, sys, os, operator
 
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

playerStats = {} 

with open(filename) as f:
	for line in f:
		line = line.strip()
		matchObj = re.search(r"^(\w+ \w+) batted (\d+) times with (\d+) hits and (\d+) runs", line)
		if matchObj:
			player = matchObj.group(1)
			atBat = int(matchObj.group(2))
			hit = int(matchObj.group(3))
			if player in playerStats:
				playerStats[player] =(playerStats[player][0] + atBat, playerStats[player][1] + hit, (playerStats[player][1] + hit) / (playerStats[player][0] + atBat))
			if player not in playerStats:
				hit=hit/1.0
				atBat=atBat/1.0
				avg = hit/atBat
				playerStats[player] = (atBat, hit, avg)

a = {}
for player in playerStats:
	a[player] = playerStats[player][2]

averages = sorted(a.iteritems(), key=operator.itemgetter(1), reverse=True) 

for i, (player, avg) in enumerate(averages):
	print "%s: %.3f" % (player, a[player])